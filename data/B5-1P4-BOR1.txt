HK offshore LNG Term. Project; Pile: B5-1P4-1 Test: 16-Dec-2020 11:33
BOR-MHU1200S; Blow: 1 CAPWAP(R) 2014-3
OP: sx,sf
Page 2 Analysis: 18-Dec-2020
CAPWAP SUMMARY RESULTS
Total CAPWAP Capacity:  41746.8; along Shaft  25456.7; at Toe  16290.1  kN
Soil Dist. Depth Ru Force Sum Unit Unit Quake
Sgmnt Below Below in Pile of Resist. Resist.
No. Gages Grade Ru (Depth) (Area)
m m kN kN kN kN/m kPa mm
 41746.8
1 27.2 1.1 300.9 41445.9 300.9 273.69 47.61 1.0
2 29.2 3.1 330.0 41115.9 630.9 163.94 28.52 1.0
3 31.2 5.1 325.7 40790.2 956.6 161.80 28.14 1.0
4 33.2 7.1 352.8 40437.4 1309.4 175.27 30.49 1.0
5 35.2 9.2 285.0 40152.4 1594.4 141.59 24.63 1.0
6 37.2 11.2 212.0 39940.4 1806.4 105.32 18.32 1.0
7 39.3 13.2 225.8 39714.6 2032.2 112.18 19.51 1.0
8 41.3 15.2 239.9 39474.7 2272.1 119.18 20.73 1.0
9 43.3 17.2 144.6 39330.1 2416.7 71.84 12.50 1.0
10 45.3 19.2 158.3 39171.8 2575.0 78.64 13.68 1.0
11 47.3 21.2 172.1 38999.7 2747.1 85.50 14.87 1.0
12 49.3 23.2 223.9 38775.8 2971.0 111.23 19.35 1.0
13 51.3 25.3 246.3 38529.5 3217.3 122.36 21.28 1.0
14 53.3 27.3 341.3 38188.2 3558.6 169.55 29.49 1.0
15 55.4 29.3 451.3 37736.9 4009.9 224.20 39.00 1.0
16 57.4 31.3 666.3 37070.6 4676.2 331.01 57.58 1.0
17 59.4 33.3 855.7 36214.9 5531.9 425.10 73.94 1.0
18 61.4 35.3 1398.7 34816.2 6930.6 694.86 120.87 1.0
19 63.4 37.3 1539.3 33276.9 8469.9 764.71 133.02 1.0
20 65.4 39.3 1777.5 31499.4 10247.4 883.04 153.60 1.0
21 67.4 41.4 1739.4 29760.0 11986.8 864.12 150.31 1.0
22 69.4 43.4 1794.3 27965.7 13781.1 891.39 155.05 1.0
23 71.5 45.4 1916.2 26049.5 15697.3 951.95 165.59 1.0
24 73.5 47.4 1923.7 24125.8 17621.0 955.68 166.23 1.0
25 75.5 49.4 1951.4 22174.4 19572.4 969.44 168.63 1.0
26 77.5 51.4 1438.2 20736.2 21010.6 714.48 124.28 1.0
27 79.5 53.4 987.1 19749.1 21997.7 490.38 85.30 1.0
28 81.5 55.4 650.4 19098.7 22648.1 323.11 56.20 1.0
29 83.5 57.5 650.4 18448.3 23298.5 323.11 56.20 1.0
30 85.5 59.5 650.4 17797.9 23948.9 323.11 56.20 1.0
31 87.6 61.5 753.9 17044.0 24702.8 374.53 65.15 1.0
32 89.6 63.5 753.9 16290.1 25456.7 374.53 65.15 1.0
Avg. Shaft 795.5 400.89 69.73 1.0
Toe 16290.1 64553.60 10.9
Soil Model Parameters/Extensions Shaft Toe
Smith Damping Factor 0.23 0.40
Case Damping Factor 0.57 0.62
Damping Type Viscous Sm+Visc
Unloading Quake (% of loading quake) 30 30
Reloading Level (% of Ru) 100 100
Unloading Level (% of Ru) 9
Resistance Gap (included in Toe Quake) (mm) 1.3
Soil Plug Weight (kN) 8.934
Soil Support Dashpot   0.740   0.000
Soil Support Weight (kN)   57.86    0.00
CAPWAP match quality =    3.88 (Wave Up Match); RSA = 0
Observed: Final Set =     4.0 mm; Blow Count =     250 b/m
Computed: Final Set =     3.6 mm; Blow Count =     277 b/m
Transducer F1 (O107)  CAL: 147.7; RF: 1.22; F3 (O105)  CAL: 145.3; RF: 1.12
A2 (K5960) CAL:   328; RF: 1.02; A4 (K5959) CAL:   356; RF: 1.02
