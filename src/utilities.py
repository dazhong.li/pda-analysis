import yaml
import re
import openpyxl as xl
import pandas as pd
import json
import numpy as np
from itertools import cycle

class Struct:
    def __init__(self, **entries):
        self.__dict__.update(entries)


def read_input_file(filename):
    ''' this function reads a yaml file in and return a dictionary'''
    if filename.endswith('.yaml'):
        return _read_yaml_file(filename)
    if filename.endswith('.json'):
        return _read_json_file(filename)

def _read_yaml_file(filename):
    with open(filename) as fin:
        file = fin.read()
        param = yaml.load(file, Loader =yaml.FullLoader)
    return Struct(**_convert_data(param))

def _read_json_file(filename):
    with open(filename,'r') as fin:
        data = json.load(fin)
    return _convert_data(data)

def _convert_data(data):
    for key in data:
        if isinstance(data[key],dict):
            try:
                data[key] = pd.DataFrame(data[key])
            except:
                data[key] = pd.DataFrame(data[key], index =[0])
        elif isinstance(data[key],list):
            data[key] = np.array(data[key])
    return data

marker_list = cycle(['circle', 'square', 'diamond', 'star', 'circle-x','hash'])
color_list=cycle(['black', 'blue','green','red','yellow'])