import src.utilities as utl
import re
import openpyxl as xl
import pandas as pd


def table_of_results(filename):
    df = pd.read_excel(filename, skiprows=6, nrows=32)
    df.dropna(how='all', axis=1, inplace=True)

    column_list = ['No', 'dist_below_gauge', 'depth_below_grade',
                  'Ru', 'force_in_pile', 'sum_Ru', 'unit_resit', 'shaft_friction', 'Quake']
    df.columns = [column_list[i] for i in range(df.shape[1])]
    df = df.set_index('No')
    return df


# def read_pad_excel(filename):
#     data = {}
#     pda_excel = xl.load_workbook(filename)
#     ws = pda_excel['Table 1']
#     df_pda_detail = table_of_results(filename)
#     PDA_test_name = re.findall(r'(\w\d-.*-.*).xlsx', filename)[0]
#     # ----Table of PDA Input------------
#     df_PDA_input = pd.read_excel(filename, skiprows=40, nrows=10)
#     df_PDA_input.drop(
#         df_PDA_input.columns[(df_PDA_input.iloc[0,:]).isna()], axis=1, inplace=True)
#     df_PDA_input.columns = ['Item', 'Shaft', 'Toe']
#     data['PDA Input'] = df_PDA_input.to_dict()
#     # ----Table of Details ------------
#     data['Table of result'] = df_pda_detail.to_dict()
#     # ----Summary of Capacity ------------
#     data['Analysis Result'] = ws.cell(53, 1).value
#     job_info_str = ws.cell(1, 1).value
#     data['Hammer info'] = re.findall(r'MHU\d+\w', job_info_str)
#     data['Test date'] = re.findall(
#         r'\d{2}-\w{3}-\d{4} \d{2}\:\d{2}', job_info_str)[0]
#     data['Pile info'] = re.findall(r'data/(.*).xlsx', filename)
#     return data

def  read_pda_data(filename):
    with open(filename) as fin:
        f = fin.read()
    # get the shaft friction along the pile
    pattern = re.compile('(^\d.*)\n',re.MULTILINE)
    data = pattern.findall(f)
    matrix= []
    for line in data:
        matrix.append([float(x) for x in line.split(' ')])
    df = pd.DataFrame(matrix)
    column_list = ['No', 'dist_below_gauge', 'depth_below_grade',
                'Ru', 'force_in_pile', 'sum_Ru', 'unit_resit', 'shaft_friction', 'Quake']
    df.columns = [column_list[i] for i in range(df.shape[1])]
    pattern = re.compile('Total CAPWAP Capacity:\s*(\d+\.?\d*)')
    total_capacity = float(pattern.findall(f)[0])
    pattern = re.compile('along Shaft:?\s*(\d+\.?\d*)')
    total_shaft = float(pattern.findall(f)[0])
    pattern = re.compile('at Toe:?\s*(\d+\.?\d*)')
    toe_resistance = float(pattern.findall(f)[0])
    pattern = re.compile('Test:\s*(.*)\n')
    test_date = pattern.findall(f)[0]
    data = {}
    data['test results'] = df.to_dict()
    data['test date'] = test_date
    data['total capacity'] = total_capacity
    data['toe resistance'] = toe_resistance
    return data
