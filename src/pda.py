import plotly.graph_objects as go


def plot_shaft(df, fig=None, trace_name=''):
    if fig is None:
        fig = go.Figure()
    fig.add_trace(go.Scatter(x=df.shaft_friction, y = df.depth_below_grade,name=title))
    fig.update_yaxes(range=[80,0],title='Depth below seabed(m)')
    fig.update_xaxes(range=[0,150],title='Shaft Friction(kPa)')
    fig.update_layout(width=500, height=800)